import Animal from './animal';

class Dog extends Animal {
    constructor() {
        super();
        console.log("==constructor dog==");
    }
}

const dog = new Dog();
dog.sayHello();