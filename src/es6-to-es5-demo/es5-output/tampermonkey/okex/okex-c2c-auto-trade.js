"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ==UserScript==
// @name         okex-c2c-auto-trade
// @namespace    https://www.okex.com/fiat/c2c
// @version      1.0.0
// @description  okex 点对点 usdt 自动交易
// @author       luojun
// @match        https://www.okex.com/fiat/c2c
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    (function () {
        //时间格式化
        //(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006 - 07 - 02 08: 09: 04.423
        //(new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006 - 7 - 2 8: 9: 4.18
        Date.prototype.Format = function (fmt) {
            var o = {
                "M+": this.getMonth() + 1, //月份
                "d+": this.getDate(), //日
                "h+": this.getHours(), //小时
                "m+": this.getMinutes(), //分
                "s+": this.getSeconds(), //秒
                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                "S": this.getMilliseconds() //毫秒
            };
            if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(fmt)) {
                    fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
                }
            }
            return fmt;
        };
    })();

    var WebUtils = function () {
        function WebUtils() {
            _classCallCheck(this, WebUtils);
        }

        _createClass(WebUtils, null, [{
            key: "notify",

            /**
             * 网页通知
             *
             * @static
             * @param {string} title 标题
             * @param {string} body 内容
             * @param {string} icon 图标链接
             * @param {string} sound 提示音链接
             * @memberof WebUtils
             */
            value: function notify(title, body, icon, sound) {
                Notification.requestPermission(function (result) {
                    if (result == 'granted') {
                        var notification = new Notification(title, {
                            body: body,
                            icon: icon,
                            silent: true
                        });
                        if (sound) {
                            //用h5播放
                            var okexAudio = new Audio(sound);
                            okexAudio.play();
                        }
                        notification.onclick = function () {
                            notification.close();
                        };
                    } else {
                        alert('通知提醒已禁止,如需启用，请在浏览器选项里启用');
                    }
                });
            }

            /**
             * 元素监听
             *
             * @static
             * @param {object} ele document.querySelector('')
             * @param {func} callback 回调
             * @param {boolean} [observeConfig={
             *         attributes: true,
             *         childList: true
             *     }] 监听配置
             * @returns
             * @memberof WebUtils
             */

        }, {
            key: "observe",
            value: function observe(ele, callback) {
                var observeConfig = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
                    attributes: true,
                    childList: true
                };

                var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver; //浏览器兼容
                var observer = new MutationObserver(function (mutations) {
                    //构造函数回调
                    mutations.forEach(function (record) {
                        callback(record);
                    });
                });
                observer.observe(ele, observeConfig);
                return observer;
            }

            /**
             * WebSocket连接
             *
             * @static
             * @param {string} serverIP 服务器ip
             * @param {number} serverPort 服务器端口
             * @returns WebSocket实例
             * @memberof WebUtils
             */

        }, {
            key: "webSocketConnect",
            value: function webSocketConnect(serverIP, serverPort) {
                var self = this;
                var host = "ws://" + serverIP + ":" + serverPort + "/";
                var socket = void 0;
                try {
                    socket = new WebSocket(host);
                    socket.onopen = function (msg) {
                        self.WebUtilsSocket = socket;
                        self.log("socket\u8FDE\u63A5\u6210\u529F", 1);
                    };
                    socket.onmessage = function (msg) {
                        if (typeof msg.data === "string") {
                            self.log("\u6536\u5230\u670D\u52A1\u5668\u6D88\u606F:" + msg.data, 1);
                        } else {
                            self.log("\u6D88\u606F\u7C7B\u578B\u6709\u8BEF", 1);
                        }
                    };
                    socket.onclose = function (msg) {
                        self.log("socket\u8FDE\u63A5\u5173\u95ED", 1);
                    };
                } catch (error) {
                    self.log(error);
                }
                return socket;
            }

            /**
             * WebSocket发送消息
             *
             * @static
             * @param {any} socket WebSocket实例
             * @param {string} msg 消息
             * @memberof WebUtils
             */

        }, {
            key: "webSocketSend",
            value: function webSocketSend(socket, msg) {
                if (socket) {
                    var endTokens = '\0';
                    msg = "" + msg + endTokens;
                    socket.send(msg);
                }
            }

            /**
             * 日志
             *
             * @static
             * @param {string} msg 日志信息
             * @param {number} [logLever=2] 0<1<2<3<4<5 => ALL < DEBUG < INFO < WARN < ERROR < FATAL
             * @memberof WebUtils
             */

        }, {
            key: "log",
            value: function log(msg) {
                var logLever = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

                var self = this;
                if ((typeof msg === "undefined" ? "undefined" : _typeof(msg)) === 'object') {
                    msg = JSON.stringify(msg);
                }
                console.log(new Date().Format("yyyy-MM-dd hh:mm:ss.S") + ":" + msg);
                if (logLever > 1 && !!self.WebUtilsSocket && self.WebUtilsSocket.readyState === 1) {
                    self.webSocketSend(self.WebUtilsSocket, "jslog:" + msg);
                }
            }

            /**
             * 异步请求
             *
             * @static
             * @param {function} func 需异步请求的函数，剩余参数传函数所需参数
             * @returns
             * @memberof WebUtils
             */

        }, {
            key: "taskRun",
            value: function taskRun(func) {
                var _arguments = arguments;

                return new Promise(function (resolve, reject) {
                    if (typeof func !== 'function') {
                        reject("\u51FD\u6570\u6709\u8BEF");
                        return;
                    }
                    setTimeout(function () {
                        try {
                            var thatArgs = [].concat(Array.prototype.slice.call(_arguments));
                            var funcArgs = thatArgs.splice(1);
                            resolve(func.apply(undefined, _toConsumableArray(funcArgs)));
                        } catch (error) {
                            reject(error);
                        }
                    }, 0);
                });
            }
        }]);

        return WebUtils;
    }();

    var OkexHelper = function () {

        /**
         * Creates an instance of OkexHelper.
         * @param {number} [autoBuyPrice=0] 小于等于时自动买入
         * @param {number} [autoSellPrice=99999999999] 大于等于时自动卖出
         * @param {number} [diffPrice=99999999999] 买1买2或卖1卖2存在价格大于等于配置,且买1大于卖1时，自动买卖
         * @param {number} [maxAutoTradeAmt=0] 最大交易额度/人民币
         * @param {string} [icon='https://img.bafang.com/v_20180123212400/okex/image/common/logo_okex_v2.png'] 提示icon
         * @param {string} [sound='http://data.huiyi8.com/yinxiao/mp3/83766.mp3'] 提示音配置
         * @param {boolean} [isDebug=false] 是否调试模式
         * @memberof OkexHelper
         */
        function OkexHelper(_ref) {
            var _ref$autoBuyPrice = _ref.autoBuyPrice,
                autoBuyPrice = _ref$autoBuyPrice === undefined ? 0 : _ref$autoBuyPrice,
                _ref$autoSellPrice = _ref.autoSellPrice,
                autoSellPrice = _ref$autoSellPrice === undefined ? 99999999999 : _ref$autoSellPrice,
                _ref$diffPrice = _ref.diffPrice,
                diffPrice = _ref$diffPrice === undefined ? 99999999999 : _ref$diffPrice,
                _ref$maxAutoTradeAmt = _ref.maxAutoTradeAmt,
                maxAutoTradeAmt = _ref$maxAutoTradeAmt === undefined ? 0 : _ref$maxAutoTradeAmt,
                _ref$icon = _ref.icon,
                icon = _ref$icon === undefined ? 'https://img.bafang.com/v_20180123212400/okex/image/common/logo_okex_v2.png' : _ref$icon,
                _ref$sound = _ref.sound,
                sound = _ref$sound === undefined ? 'http://data.huiyi8.com/yinxiao/mp3/83766.mp3' : _ref$sound,
                _ref$isAutoSell = _ref.isAutoSell,
                isAutoSell = _ref$isAutoSell === undefined ? true : _ref$isAutoSell,
                _ref$isDebug = _ref.isDebug,
                isDebug = _ref$isDebug === undefined ? false : _ref$isDebug,
                _ref$finishRate = _ref.finishRate,
                finishRate = _ref$finishRate === undefined ? 0.8 : _ref$finishRate,
                _ref$startOfJob = _ref.startOfJob,
                startOfJob = _ref$startOfJob === undefined ? '08:30' : _ref$startOfJob,
                _ref$endOfJob = _ref.endOfJob,
                endOfJob = _ref$endOfJob === undefined ? '23:59' : _ref$endOfJob,
                _ref$autoRefreshInter = _ref.autoRefreshInterval,
                autoRefreshInterval = _ref$autoRefreshInter === undefined ? 60 * 60 : _ref$autoRefreshInter;

            _classCallCheck(this, OkexHelper);

            this.config = {
                autoBuyPrice: autoBuyPrice,
                autoSellPrice: autoSellPrice,
                diffPrice: diffPrice,
                maxAutoTradeAmt: maxAutoTradeAmt,
                icon: icon,
                sound: sound,
                isAutoSell: isAutoSell,
                isDebug: isDebug,
                attributes: true,
                childList: true,
                orderTradeBuyObserve: null,
                orderTradeSellObserve: null,
                finishRate: finishRate,
                canContinuedBuying: true, //可继续买入
                okexNotifySocket: WebUtils.webSocketConnect('127.0.0.1', 1234),
                checkUserStatusTimer: null,
                tempDiffPrice: diffPrice,
                orderStatus: {
                    UnFinish: 0,
                    Finished: 1,
                    Canceled: 2
                },
                sellFailed: false, //卖出失败
                sellFailedOrderAmount: 0, //卖出失败数量
                startOfJob: startOfJob, //工作开始时间
                startHoursOfJob: 0,
                startMinutesOfJob: 0,
                endOfJob: endOfJob, //工作结束时间
                endHoursOfJob: 0,
                endMinutesOfJob: 0,
                thawingTime: new Date(), //账号冻结后解冻时间
                availableUSDT: 0, //可用usdt
                unFinishedOrderCount: 0, //未完成订单数
                nowBuyPrice: 0, //现在购买价格
                isOrdering: false, //正在下单中
                autoRefreshInterval: autoRefreshInterval, //自动刷新时间间隔 单位/秒
                lastRefreshTime: Date.now()
            };
            var startOfJobArr = this.config.startOfJob.split(':');
            this.config.startHoursOfJob = Number(startOfJobArr[0]);
            this.config.startMinutesOfJob = Number(startOfJobArr[1]);
            var endOfJobArr = this.config.endOfJob.split(':');
            this.config.endHoursOfJob = Number(endOfJobArr[0]);
            this.config.endMinutesOfJob = Number(endOfJobArr[1]);
        }

        /**
         * okex网页通知
         *
         * @param {string} title 标题
         * @param {string} body 内容
         * @memberof OkexHelper
         */


        _createClass(OkexHelper, [{
            key: "okexNotify",
            value: function okexNotify(title, body) {
                var self = this;
                WebUtils.notify(title, body, self.config.icon, self.config.sound);
            }

            /**
             * 发送提醒(邮件、短信、qq等)
             *
             * @param {string} msg 提醒内容
             * @param {string} [type='autotrade']  提醒类型 autotrade:自动交易提醒
             * @memberof OkexHelper
             */

        }, {
            key: "sendReminder",
            value: function sendReminder(msg) {
                var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'autotrade';

                var self = this;
                WebUtils.webSocketSend(self.config.okexNotifySocket, type + ":" + msg);
            }

            /**
             * 转换为数字
             *
             * @param {string} str 有千分位等的数字字符串
             * @param {number} [length=2] 四舍五入后长度
             * @returns
             * @memberof OkexHelper
             */

        }, {
            key: "toNum",
            value: function toNum(str) {
                var length = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

                var self = this;
                return self.mathRound(Number(str.split(',').join('')), length);
            }

            /**
             * 四舍五入
             *
             * @param {number} num 原数字
             * @param {number} [length=2] 四舍五入后长度
             * @returns
             * @memberof OkexHelper
             */

        }, {
            key: "mathRound",
            value: function mathRound(num) {
                var length = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

                return parseFloat(num.toFixed(length));
            }

            /**
             * 自动交易
             *
             * @memberof OkexHelper
             */

        }, {
            key: "autoOrderTrade",
            value: function autoOrderTrade() {
                var self = this;
                //配置通知
                var configNotifyMsg = "\u4EF7\u683C\u5C0F\u4E8E\u7B49\u4E8E" + self.config.autoBuyPrice + "\u65F6\u81EA\u52A8\u4E70\u5165\r\n";
                if (self.config.isAutoSell) {
                    configNotifyMsg += "\u4EF7\u683C\u5927\u4E8E\u7B49\u4E8E" + self.config.autoSellPrice + "\u65F6\u81EA\u52A8\u5356\u51FA\r\n";
                }
                configNotifyMsg += "\u4E70\u5356\u4EF7\u683C\u5DEE\u5F02\u5927\u4E8E\u7B49\u4E8E" + self.config.diffPrice + "\u65F6\u81EA\u52A8\u4E70\u5356\r\n\u4E70\u5356\u6700\u5927\u91D1\u989D\u4E3A\uFF1A\uFFE5" + self.config.maxAutoTradeAmt;
                WebUtils.log(configNotifyMsg);
                self.okexNotify('Okex-C2C自动买卖配置提醒', configNotifyMsg);
                var _oldFetch = window.fetch;
                window.fetch = function () {
                    var reqArgs = arguments;
                    var newFetch = _oldFetch.apply(window, arguments).then(function (res) {
                        return res.json().then(function (json) {
                            self.handleResponse(reqArgs, json);
                            //覆盖原json函数，解决流已读问题
                            res.json = function () {
                                return json;
                            };
                            return res;
                        });
                    });
                    return newFetch;
                };
            }
        }, {
            key: "handleResponse",
            value: function handleResponse(reqArgs, result) {
                var self = this;
                if (!reqArgs || !result || result.code !== 0) {
                    WebUtils.log(JSON.stringify(reqArgs), 4);
                    WebUtils.log(JSON.stringify(result), 4);
                    if (result.Code === 403) {
                        self.sendReminder('登录状态已失效，请及时重试登录');
                    }
                    return;
                }
                var reqUrl = reqArgs[0];
                var reqParams = reqArgs[1];
                var UrlConst = self.getUrlConst();
                //用户资产
                var userAssetUrl = UrlConst.GET_USER_ASSET.replace('{0}', 'usdt');
                if (reqUrl.includes(userAssetUrl)) {
                    self.handleUserAsset(result);
                }
                //交易监听
                if (reqUrl.includes(UrlConst.GET_TRADING_ORDER)) {
                    self.handleTradeOrder(result);
                }
                //未完成订单
                if (reqUrl.includes("/c2c-open/orders?status=0")) {
                    self.handleUnFinishedOrder(result);
                    console.clear();
                }
            }

            /**
             * 用户资产
             *
             * @param {any} result
             * @memberof OkexHelper
             */

        }, {
            key: "handleUserAsset",
            value: function handleUserAsset(result) {
                var self = this;
                if (self.config.isDebug) {
                    debugger;
                }
                self.config.availableUSDT = Math.round(result.data.available);
            }

            /**
             * 未完成订单数记录
             *
             * @param {any} result
             * @memberof OkexHelper
             */

        }, {
            key: "handleUnFinishedOrder",
            value: function handleUnFinishedOrder(result) {
                var self = this;
                if (self.config.isDebug) {
                    debugger;
                }
                self.config.unFinishedOrderCount = 0;
                var notifyMsg = 'Okex C2C 交易 ';
                var isNotify = false;
                result.data.items.forEach(function (unFinishedOrder) {
                    if (unFinishedOrder.buy) {
                        self.config.unFinishedOrderCount += 1;
                    }
                    var notifyTimeInterval = 60 * 3; //3分钟一次
                    //卖出失败的情况
                    if (result.data.items.length === 1 && unFinishedOrder.buy && unFinishedOrder.paidDate === 0) {
                        var nowBuyPrice = self.config.nowBuyPrice;
                        //低价买入，且未付款时，定时邮件通知付款
                        if (unFinishedOrder.exchangeRate < nowBuyPrice && unFinishedOrder.expiredAfterSeconds % notifyTimeInterval < 8) {
                            var msg = "Okex C2C \u4EA4\u6613\u8BA2\u5355\uFF1A" + unFinishedOrder.publicOrderId + "\uFF0C\u4E70\u5165\u4EF7\uFF1A\uFFE5" + unFinishedOrder.exchangeRate + ",\u73B0\u4EF7\uFF1A\uFFE5" + nowBuyPrice + ",\u5B58\u5728\u5229\u6DA6\uFF0C\u8BF7\u53CA\u65F6\u4ED8\u6B3E\u4E70\u5165";
                            WebUtils.log(msg);
                            if (!self.config.isDebug) {
                                self.sendReminder(msg);
                            }
                        }
                    }
                    //成功买卖，未付款
                    if (result.data.items.length > 1) {
                        notifyMsg += "\u8BA2\u5355\uFF1A" + unFinishedOrder.publicOrderId + "\uFF0C" + (unFinishedOrder.buy ? '买入' : '卖出') + "\u4EF7\uFF1A\uFFE5" + unFinishedOrder.exchangeRate + "," + (unFinishedOrder.paidDate === 0 ? '未付款;' : '已付款;');
                        if (unFinishedOrder.buy) {
                            var sellerAccount = unFinishedOrder.sellerAllReceiptAccountList[0];
                            notifyMsg += "\u5356\u5BB6:" + sellerAccount.accountName + "\uFF0C\u5361\u53F7\uFF1A" + sellerAccount.accountNo + ",\u6536\u6B3E\u94F6\u884C\uFF1A" + sellerAccount.bankName + ";";
                            if (unFinishedOrder.expiredAfterSeconds > 0 && unFinishedOrder.expiredAfterSeconds % notifyTimeInterval < 8) {
                                isNotify = true;
                            }
                        }
                    }
                });
                if (isNotify) {
                    WebUtils.log(notifyMsg);
                    if (!self.config.isDebug) {
                        self.sendReminder(notifyMsg);
                    }
                }
            }

            /**
             * 交易
             *
             * @param {any} result
             * @returns
             * @memberof OkexHelper
             */

        }, {
            key: "handleTradeOrder",
            value: function handleTradeOrder(result) {
                var _this = this;

                var self = this;
                //是否选中了usdt
                var activeCoin = document.querySelector('.ok-left-menu .list-container li.active label.pair').innerText;
                if (activeCoin !== 'USDT') {
                    console.log('请先在左侧选中USDT交易');
                    return;
                }
                var dateNow = new Date();
                var overSeconds = Math.ceil((dateNow - self.config.lastRefreshTime) / 1000);
                //自动刷新
                if (overSeconds >= self.config.autoRefreshInterval) {
                    window.location.reload();
                    return;
                }
                if (dateNow < self.config.thawingTime) {
                    WebUtils.log("\u8D26\u53F7\u51BB\u7ED3\u4E2D,\u7B49\u5F85\u8D26\u53F7\u89E3\u51BB\u540E\u8FDB\u884C\u4EA4\u6613\u8BA1\u7B97", 1);
                    return;
                }
                var startDateOfJob = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), self.config.startHoursOfJob, self.config.startMinutesOfJob, 0, 0);
                var endDateOfJob = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), self.config.endHoursOfJob, self.config.endMinutesOfJob, 0, 0);
                if (dateNow < startDateOfJob || dateNow > endDateOfJob) {
                    WebUtils.log("\u975E\u5DE5\u4F5C\u65F6\u95F4\u6BB5(" + self.config.startOfJob + "-" + self.config.endOfJob + ")\u5185\uFF0C\u7B49\u5F85\u5DE5\u4F5C\u65F6\u95F4\u5F00\u59CB\u540E\u8FDB\u884C\u4EA4\u6613\u8BA1\u7B97", 1);
                    return;
                }
                var buyTradingOrders = result.data.buyTradingOrders;
                var sellTradingOrders = result.data.sellTradingOrders;
                if (buyTradingOrders && buyTradingOrders.length > 0 && sellTradingOrders && sellTradingOrders.length > 0) {
                    var _ret = function () {
                        if (self.config.isDebug) {
                            debugger;
                        }
                        //未完成订单判断
                        if (self.config.unFinishedOrderCount === 2) {
                            WebUtils.log("\u5B58\u5728" + self.config.unFinishedOrderCount + "\u5355\u672A\u5B8C\u6210\u8BA2\u5355\uFF0C\u4E0D\u80FD\u81EA\u52A8\u4E70\u5356", 1);
                            return {
                                v: void 0
                            };
                        } else if (self.config.unFinishedOrderCount === 0 && self.config.sellFailed) {
                            //订单已完成或已取消
                            self.config.sellFailed = false;
                            self.config.sellFailedOrderAmount = 0;
                            self.config.diffPrice = self.config.tempDiffPrice;
                            WebUtils.log("\u8BA2\u5355\u5DF2\u5B8C\u6210\u6216\u5DF2\u53D6\u6D88\uFF0C\u73B0\u5DF2\u5C06\u5DEE\u5F02\u4EF7\u81EA\u52A8\u5207\u6362\u56DE" + self.config.diffPrice);
                        }

                        var buy1Order = sellTradingOrders[sellTradingOrders.length - 1];
                        var buy1Price = buy1Order.exchangeRate;
                        self.config.nowBuyPrice = buy1Price;
                        var buy1OrderAmount = buy1Order.availableAmount; //不能四舍五入，会超出订单金额
                        var buy1OrderTotal = buy1Price * buy1OrderAmount;
                        if (buy1OrderTotal > self.config.maxAutoTradeAmt) {
                            buy1OrderTotal = self.config.maxAutoTradeAmt;
                            buy1OrderAmount = parseInt(buy1OrderTotal / buy1Price * 100) / 100;
                        }
                        var thatDiffPrice = 0;
                        var _resCallback = function resCallback(successfulBuyAmount) {
                            if (successfulBuyAmount > 0) {
                                var msg = "Okex C2C \u4EA4\u6613\u6210\u529F\u4E70\u5165USDT:" + successfulBuyAmount + "\u4E2A\uFF0C\u8BF7\u53CA\u65F6\u4ED8\u6B3E";
                                if (!self.config.isDebug) {
                                    self.sendReminder(msg);
                                }
                            }
                        };
                        var worthBuying = false; //是否值得买

                        var _loop = function _loop(i) {
                            var sellOrder = buyTradingOrders[i];;
                            var sellPrice = sellOrder.exchangeRate;
                            var sellOrderAmount = sellOrder.availableAmount;
                            thatDiffPrice = self.mathRound(sellPrice - buy1Price);
                            var sellTotalOrderQty = sellOrder.clientCompletedOrderQuantity + sellOrder.clientCancelledOrderQuantity;
                            var sellFinishRate = sellTotalOrderQty > 0 ? self.mathRound(sellOrder.clientCompletedOrderQuantity / sellTotalOrderQty * 100) : 0;
                            var sellOrderId = sellOrder.publicTradingOrderId;
                            var thatSellOrderAmount = 0;
                            //可用usdt
                            var openTradeCurrencyAvailable = self.config.availableUSDT;
                            //跑路模式
                            if (self.config.isAutoSell && sellPrice >= self.config.autoSellPrice && sellFinishRate >= self.config.finishRate * 100) {
                                thatSellOrderAmount = sellOrderAmount > openTradeCurrencyAvailable ? openTradeCurrencyAvailable : sellOrderAmount;
                                if (thatSellOrderAmount > 0) {
                                    self.createOrder({
                                        tradePrice: sellPrice,
                                        tradeType: 1,
                                        diffPrice: thatDiffPrice,
                                        orderId: sellOrderId,
                                        orderAmount: thatSellOrderAmount,
                                        resCallback: function resCallback(successfulSellAmount) {
                                            var msg = '';
                                            if (successfulSellAmount > 0) {
                                                msg = "Okex C2C \u4EA4\u6613\u4EE5\uFFE5" + sellPrice + "\u8DD1\u8DEF\u6210\u529FUSDT\uFF1A" + thatSellOrderAmount + "\u4E2A\uFF0C\u8BF7\u53CA\u65F6\u6536\u6B3E\u653E\u5E01";
                                            } else {
                                                msg = "Okex C2C \u4EA4\u6613\u4EE5\uFFE5" + sellPrice + "\u8DD1\u8DEF\u5931\u8D25";
                                            }
                                            if (!self.config.isDebug) {
                                                self.sendReminder(msg);
                                            }
                                        }
                                    });
                                    WebUtils.log("\u5F53\u524D\u5356\u51FA\u4EF7\uFFE5" + sellPrice + ",\u5356\u51FAUSDT:" + sellOrderAmount + "\u4E2A,\u51C6\u5907\u8DD1\u8DEF\u4E86");
                                } else {
                                    WebUtils.log("\u5F53\u524D\u5356\u4EF7\uFF1A" + sellPrice + ",USDT:" + sellOrderAmount + "\u4E2A\uFF0C\u53EF\u60DC\u6CA1\u5E01\u4E86");
                                }
                                return {
                                    v: {
                                        v: void 0
                                    }
                                };
                            }
                            if (self.config.unFinishedOrderCount === 0 && thatDiffPrice >= self.config.diffPrice || self.config.unFinishedOrderCount === 1 && thatDiffPrice >= self.config.diffPrice * 5) {
                                //存在1单未完成订单时，有5倍利润的话也买
                                if (sellFinishRate < self.config.finishRate * 100) {
                                    WebUtils.log("\u5356\u5BB6\uFF1A" + sellOrder.clientName + "\uFF0C\u4EE5\uFFE5" + sellOrder.exchangeRate + "\u4E70\u5165USDT:" + sellOrder.availableAmount + ",\u5B8C\u6210\u5355\u6570:" + sellOrder.clientCompletedOrderQuantity + ",\u5B8C\u6210\u7387:" + sellFinishRate + "%\uFF0C\u7591\u4F3C\u9ED1\u540D\u5355\u7528\u6237\uFF0C\u5DF2\u505C\u6B62\u8BA1\u7B97\u6B64\u6B21\u4EA4\u6613\u4FE1\u606F");
                                    return "continue";
                                }
                                if (self.config.diffPrice > 0 && i < 5 && sellPrice - buyTradingOrders[i + 1].exchangeRate >= self.config.diffPrice * 2) {
                                    WebUtils.log("\u5356\u5BB6\uFF1A" + sellOrder.clientName + "\uFF0C\u4EE5\uFFE5" + sellOrder.exchangeRate + "\u4E70\u5165USDT:" + sellOrder.availableAmount + ",\u5B8C\u6210\u5355\u6570:" + sellOrder.clientCompletedOrderQuantity + ",\u5B8C\u6210\u7387:" + sellFinishRate + "%\uFF0C\u5F53\u524D\u4E701\u4EF7\u4E3A\uFF1A\uFFE5" + buy1Price + ",\u5356" + (i + 2) + "\u4EF7\u4E3A:\uFFE5" + buyTradingOrders[i + 1].exchangeRate + "\uFF0C\u6781\u5927\u51E0\u7387\u662F\u5751\uFF0C\u5DF2\u505C\u6B62\u8BA1\u7B97\u6B64\u6B21\u4EA4\u6613\u4FE1\u606F");
                                    return "continue";
                                }
                                //收益最大化,寻找最高金额的
                                if (i === 0 && buy1OrderAmount > sellOrderAmount && sellOrderAmount < openTradeCurrencyAvailable) {
                                    var sellOrder2 = buyTradingOrders[1];
                                    var sellPrice2 = sellOrder2.exchangeRate;
                                    var sellOrderAmount2 = sellOrder2.availableAmount;
                                    var sell2TotalOrderQty = sellOrder2.clientCompletedOrderQuantity + sellOrder2.clientCancelledOrderQuantity;
                                    var sellFinishRate2 = sell2TotalOrderQty > 0 ? self.mathRound(sellOrder2.clientCompletedOrderQuantity / sell2TotalOrderQty * 100) : 0;;
                                    if (sellPrice2 >= sellPrice && sellFinishRate2 >= self.config.finishRate * 100 && sellOrderAmount2 > sellOrderAmount) {
                                        WebUtils.log("\u53D1\u73B0\u66F4\u9AD8\u6536\u76CA\uFF0C\u539F\u5356\u51FA\u4EF7\uFFE5" + sellPrice + ",\u65B0\u5356\u51FA\u4EF7:\uFFE5" + sellPrice2 + ",\u539FUSDT\u4E2A\u6570:" + sellOrderAmount + ",\u65B0USDT\u4E2A\u6570:" + sellOrderAmount2);
                                        sellOrder = sellOrder2;
                                        sellPrice = sellPrice2;
                                        sellOrderAmount = sellOrderAmount2;
                                    }
                                    if (sellOrderAmount < openTradeCurrencyAvailable) {
                                        var sellOrder3 = buyTradingOrders[2];
                                        var sellPrice3 = sellOrder3.exchangeRate;
                                        var sellOrderAmount3 = sellOrder3.availableAmount;
                                        var sell3TotalOrderQty = sellOrder3.clientCompletedOrderQuantity + sellOrder3.clientCancelledOrderQuantity;
                                        var sellFinishRate3 = sell3TotalOrderQty > 0 ? self.mathRound(sellOrder3.clientCompletedOrderQuantity / sell3TotalOrderQty * 100) : 0;;
                                        if (sellPrice3 >= sellPrice && sellFinishRate3 >= self.config.finishRate * 100 && sellOrderAmount3 > sellOrderAmount) {
                                            WebUtils.log("\u53D1\u73B0\u66F4\u9AD8\u6536\u76CA\uFF0C\u539F\u5356\u51FA\u4EF7\uFFE5" + sellPrice + ",\u65B0\u5356\u51FA\u4EF7:\uFFE5" + sellPrice3 + ",\u539FUSDT\u4E2A\u6570:" + sellOrderAmount + ",\u65B0USDT\u4E2A\u6570:" + sellOrderAmount3);
                                            sellOrder = sellOrder3;
                                            sellPrice = sellPrice3;
                                            sellOrderAmount = sellOrderAmount3;
                                        }
                                    }
                                }
                                //卖出金额
                                buy1OrderAmount = buy1OrderAmount <= openTradeCurrencyAvailable || buy1Price <= _this.config.autoBuyPrice ? buy1OrderAmount : openTradeCurrencyAvailable;
                                if (sellOrderAmount >= buy1OrderAmount) {
                                    thatSellOrderAmount = buy1OrderAmount;
                                } else if (buy1Price <= _this.config.autoBuyPrice) {
                                    thatSellOrderAmount = 0;
                                } else {
                                    thatSellOrderAmount = sellOrderAmount;
                                    buy1OrderAmount = sellOrderAmount;
                                    buy1OrderTotal = buy1OrderAmount * buy1Price;
                                }
                                if (buy1OrderAmount === 0 && thatSellOrderAmount === 0) {
                                    WebUtils.log("\u4E70\u5356\u91D1\u989D\u4E0D\u80FD\u4E3A0");
                                    return {
                                        v: {
                                            v: void 0
                                        }
                                    };
                                }
                                _resCallback = function resCallback(successfulBuyAmount) {
                                    if (thatSellOrderAmount > 0 && successfulBuyAmount > 0) {
                                        //卖出
                                        self.createOrder({
                                            tradePrice: sellPrice,
                                            tradeType: 1,
                                            diffPrice: thatDiffPrice,
                                            orderId: sellOrderId,
                                            orderAmount: successfulBuyAmount,
                                            resCallback: function resCallback(successfulSellAmount) {
                                                var msg = '';
                                                if (successfulSellAmount > 0) {
                                                    msg = "Okex C2C \u4EA4\u6613\u4EE5\uFFE5" + buy1Price + "\u4E70\u5165USDT\uFF1A" + successfulBuyAmount + "\u4E2A\uFF0C\u4EE5\uFFE5" + sellPrice + "\u5356\u51FAUSDT\uFF1A" + successfulSellAmount + "\u4E2A\uFF0C\u8BF7\u53CA\u65F6\u6536\u6B3E\u53CA\u4ED8\u6B3E";
                                                    if (self.config.sellFailed) {
                                                        msg = "Okex C2C \u4EA4\u6613\u4EE5\uFFE5" + sellPrice + "\u5356\u51FAUSDT\uFF1A" + successfulSellAmount + "\u4E2A\uFF0C\u8BF7\u53CA\u65F6\u6536\u6B3E\u53CA\u4ED8\u6B3E";
                                                        self.config.sellFailed = false;
                                                        self.config.sellFailedOrderAmount = 0;
                                                        self.config.diffPrice = self.config.tempDiffPrice;
                                                        WebUtils.log("\u5356\u51FA\u6210\u529F\uFF0C\u73B0\u5DF2\u5C06\u5DEE\u5F02\u4EF7\u81EA\u52A8\u5207\u6362\u56DE" + self.config.diffPrice);
                                                    }
                                                } else {
                                                    msg = "Okex C2C \u4EA4\u6613\u4EE5\uFFE5" + buy1Price + "\u4E70\u5165USDT\uFF1A" + successfulBuyAmount + "\u4E2A\uFF0C\u4EE5\uFFE5" + sellPrice + "\u5356\u51FA\u5931\u8D25\uFF0C\u8BF7\u53CA\u65F6\u4ED8\u6B3E\u53CA\u5356\u51FA";
                                                    if (self.config.sellFailed) {
                                                        msg = "Okex C2C \u4EA4\u6613\u4EE5\uFFE5" + sellPrice + "\u5356\u51FAUSDT\uFF1A" + successfulBuyAmount + "\u4E2A\u5931\u8D25\uFF0C\u8BF7\u53CA\u65F6\u5356\u51FA";
                                                    }
                                                    self.config.sellFailed = true;
                                                    self.config.sellFailedOrderAmount = successfulBuyAmount;
                                                    //TODO 暂时不切换差异价
                                                    // self.config.diffPrice = 0;
                                                    WebUtils.log("\u5356\u51FA\u5931\u8D25\uFF0C\u73B0\u5DF2\u5C06\u5DEE\u5F02\u4EF7\u81EA\u52A8\u5207\u6362\u4E3A" + self.config.diffPrice + "\u8FDB\u884C\u5356\u51FA\uFF0C\u9632\u6B62\u4E8F\u635F");
                                                }
                                                if (!self.config.isDebug) {
                                                    self.sendReminder(msg);
                                                }
                                            }
                                        });
                                        worthBuying = true;
                                    } else {
                                        self.config.autoBuyPrice -= 0.05;
                                    }
                                };
                                return "break";
                            }
                        };

                        _loop2: for (var i = 0; i < 5; i++) {
                            var _ret2 = _loop(i);

                            switch (_ret2) {
                                case "continue":
                                    continue;

                                case "break":
                                    break _loop2;

                                default:
                                    if ((typeof _ret2 === "undefined" ? "undefined" : _typeof(_ret2)) === "object") return _ret2.v;
                            }
                        }

                        WebUtils.log("buy1Price:" + buy1Price, 1);

                        if (buy1Price <= self.config.autoBuyPrice || worthBuying) {
                            if (!self.config.sellFailed || worthBuying && self.config.diffPrice > 0) {
                                var orderId = buy1Order.publicTradingOrderId;
                                //下单
                                self.createOrder({
                                    tradePrice: buy1Price,
                                    tradeType: 0,
                                    diffPrice: thatDiffPrice,
                                    orderId: orderId,
                                    orderAmount: buy1OrderAmount,
                                    resCallback: _resCallback
                                });
                            } else if (worthBuying && _resCallback && self.config.sellFailedOrderAmount > 0) {
                                _resCallback(self.config.sellFailedOrderAmount);
                            }
                        }
                    }();

                    if ((typeof _ret === "undefined" ? "undefined" : _typeof(_ret)) === "object") return _ret.v;
                }
            }

            /**
             * 下单
             *
             * @static
             * @param {number} tradePrice 交易价格
             * @param {number} tradeType 交易类型 0-买入 1-卖出
             * @param {number} diffPrice 差异价
             * @param {string} orderId 订单id
             * @param {number} orderAmount 订单金额/usdt
             * @memberof OkexHelper
             */

        }, {
            key: "createOrder",
            value: function createOrder(_ref2) {
                var tradePrice = _ref2.tradePrice,
                    tradeType = _ref2.tradeType,
                    diffPrice = _ref2.diffPrice,
                    orderId = _ref2.orderId,
                    orderAmount = _ref2.orderAmount,
                    _ref2$tryCount = _ref2.tryCount,
                    tryCount = _ref2$tryCount === undefined ? 1 : _ref2$tryCount,
                    resCallback = _ref2.resCallback;

                var self = this;
                if (self.config.isOrdering) {
                    WebUtils.log("\u6B63\u5728\u4E0B\u5355\u4E2D\uFF0C\u5E76\u53D1\u4E86...");
                    return;
                }
                self.config.isOrdering = true;
                var params = {
                    publicTradingOrderId: orderId,
                    amount: orderAmount
                };
                if (self.config.isDebug) {
                    debugger;
                    // params.publicTradingOrderId += '--debug';
                    if (resCallback) {
                        resCallback(orderAmount);
                    }
                    return;
                }
                //买入成功后先卖出再继续购买
                if (tradeType === 0 && !self.config.canContinuedBuying) {
                    return;
                }
                var notifyMsg = '';
                var urlConst = self.getUrlConst();
                self.okFetch(urlConst.POST_OPEN_ORDER, params, {
                    method: "POST"
                }).then(function (result) {
                    self.config.isOrdering = false;
                    if (self.config.isDebug) {
                        debugger;
                    }
                    if (result.code === 0) {
                        switch (tradeType) {
                            case 0:
                                notifyMsg = "\u5F53\u524D\u4E701\u4EF7\u4E3A:" + tradePrice + ",\u4E70\u5356\u5DEE\u4EF7\u4E3A\uFF1A" + diffPrice.toFixed(2) + ",\u7B2C" + tryCount + "\u6B21\u81EA\u52A8\u4E70\u5165" + orderAmount + "usdt\r\n\u81EA\u52A8\u4E70\u5165\u7ED3\u679C\uFF1A" + JSON.stringify(result);
                                WebUtils.log(notifyMsg);
                                self.okexNotify('Okex自动买入通知', notifyMsg);
                                self.config.canContinuedBuying = false;
                                self.config.unFinishedOrderCount += 1;
                                break;
                            case 1:
                                notifyMsg = "\u5F53\u524D\u53561\u4EF7\u4E3A:" + tradePrice + ",\u4E70\u5356\u5DEE\u4EF7\u4E3A\uFF1A" + diffPrice.toFixed(2) + ",\u7B2C" + tryCount + "\u6B21\u81EA\u52A8\u5356\u51FA" + orderAmount + "usdt\r\n\u81EA\u52A8\u5356\u51FA\u7ED3\u679C\uFF1A" + JSON.stringify(result);
                                WebUtils.log(notifyMsg);
                                self.okexNotify('Okex自动卖出通知', notifyMsg);
                                self.config.canContinuedBuying = true;
                                self.config.unFinishedOrderCount += 1;
                                break;
                            default:
                                WebUtils.log("\u4EA4\u6613\u7C7B\u578B\u6709\u8BEF");
                                break;
                        }
                        //先停止，每次最多两张未完成订单，成功就不买了，预留卖出位置
                        // if (tradeType === 0) {
                        //     self.config.orderTradeBuyObserve.disconnect();
                        // }
                        // if (tradeType === 1) {
                        //     self.config.orderTradeSellObserve.disconnect();
                        // }
                        //回调
                        if (resCallback) {
                            resCallback(orderAmount);
                        }
                    } else {
                        switch (tradeType) {
                            case 0:
                                notifyMsg = "\u5F53\u524D\u4E701\u4EF7\u4E3A:" + tradePrice + ",\u4E70\u5356\u5DEE\u4EF7\u4E3A\uFF1A" + diffPrice.toFixed(2) + ",\u7B2C" + tryCount + "\u6B21\u81EA\u52A8\u4E70\u5165" + orderAmount + "usdt\u5931\u8D25\r\n\u5931\u8D25\u539F\u56E0\uFF1A" + JSON.stringify(result);
                                WebUtils.log(notifyMsg);
                                // self.okexNotify('Okex自动买入通知', notifyMsg);
                                break;
                            case 1:
                                notifyMsg = "\u5F53\u524D\u53561\u4EF7\u4E3A:" + tradePrice + ",\u4E70\u5356\u5DEE\u4EF7\u4E3A\uFF1A" + diffPrice.toFixed(2) + ",\u7B2C" + tryCount + "\u6B21\u81EA\u52A8\u5356\u51FA" + orderAmount + "usdt\u5931\u8D25\r\n\u5931\u8D25\u539F\u56E0\uFF1A" + JSON.stringify(result);
                                WebUtils.log(notifyMsg);
                                // self.okexNotify('Okex自动卖出通知', notifyMsg);
                                break;
                            default:
                                WebUtils.log("\u4EA4\u6613\u7C7B\u578B\u6709\u8BEF");
                                break;
                        }
                        //您的账号因取消订单过多，今天已被限制下单
                        if (result.code === 13046) {
                            WebUtils.log(notifyMsg);
                            var dateNow = new Date();
                            //解冻时间为第二天
                            self.config.thawingTime = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), 23, 59, 59, 999);
                            return;
                        }
                        //您的下单数量超过交易单最大委托数，请重新调整
                        if (result.code === 13065 /*&& diffPrice > self.config.diffPrice * 2*/) {
                                if (tryCount <= 2) {
                                    orderAmount = parseInt(orderAmount * 100 / 2) / 100;
                                    tryCount += 1;
                                    //调整数量下单
                                    self.createOrder({
                                        tradePrice: tradePrice,
                                        tradeType: tradeType,
                                        diffPrice: diffPrice,
                                        orderId: orderId,
                                        orderAmount: orderAmount,
                                        tryCount: tryCount,
                                        resCallback: resCallback
                                    });
                                    return;
                                }
                            }
                        if (resCallback) {
                            resCallback(0);
                        }
                    }
                }).catch(function (reason) {
                    self.config.isOrdering = false;
                    if (self.config.isDebug) {
                        debugger;
                    }
                    WebUtils.log(JSON.stringify(reason), 4);
                });
            }

            /**
             * 获取okex url
             *
             * @returns 所有url
             * @memberof OkexHelper
             */

        }, {
            key: "getUrlConst",
            value: function getUrlConst() {
                var API_VERSION = "v2",
                    URL_PRE = "/" + API_VERSION,
                    URL_CONST = {
                    GET_PRODUCT_LIST: URL_PRE + "/c2c-open/currencies/group",
                    GET_TRADING_ORDER: URL_PRE + "/c2c-open/tradingOrders/group",
                    GET_USER_PHONE: URL_PRE + "/c2c-open/user/phone",
                    GET_OPEN_USER_KYC: URL_PRE + "/c2c-open/user/kyc",
                    POST_TRADING_ORDER: URL_PRE + "/c2c-open/tradingOrder",
                    GET_MY_ORDERS: URL_PRE + "/c2c-open/tradingOrders/my",
                    GET_USER_ASSET: URL_PRE + "/c2c-open/balance/{0}",
                    POST_CANCEL_MY_ORDER: URL_PRE + "/c2c-open/tradingOrder/{0}/cancel",
                    POST_OPEN_ORDER: URL_PRE + "/c2c-open/order",
                    GET_OPEN_ORDER_LIST: URL_PRE + "/c2c-open/orders",
                    POST_OPEN_CONFIRM_PAYMENT: URL_PRE + "/c2c-open/order/{0}/payment-paid",
                    POST_OPEN_CONFIRM_RECEIPT: URL_PRE + "/c2c-open/order/{0}/payment-confirmed",
                    POST_CHANGE_ORDER_ACCEPTOR: URL_PRE + "/c2c-open/tradingOrder/accept-order",
                    GET_CURRENCY_PLATFORM_FEE: URL_PRE + "/c2c-open/currency/{0}",
                    POST_CONTACT_EACH_OTHER: URL_PRE + "/c2c-open/order/privacy-number/{0}",
                    POST_CHANGE_CONTACT_NUMBER: URL_PRE + "/c2c-open/order/refresh-privacy-number/{0}",
                    GET_INDEX_TICKER: URL_PRE + "/market/index/ticker?symbol=f_usd_{0}",
                    GET_INDEX_KLINE: URL_PRE + "/market/index/kLine?symbol=f_usd_{0}&type=1min&limit=300&since=0",
                    GET_OPEN_ORDER_DETAIL: URL_PRE + "/c2c-open/order/{0}",
                    POST_FROZEN_TRADE_ORDER: URL_PRE + "/c2c-open/order/{0}/payment-rejected",
                    POST_CANCEL_ORDER: URL_PRE + "/c2c-open/order/{0}/cancel",
                    GET_FIRST_ENTRANCE: URL_PRE + "/c2c/first-entrance",
                    POST_UPDATE_FIRST_ENTRANCE: URL_PRE + "/c2c/entrance",
                    GET_ORDER_DETAIL: URL_PRE + "/c2c/order/{0}",
                    POST_INDEX_CONFIRM_PAYMENT: URL_PRE + "/c2c/order/{0}/payment-paid",
                    POST_INDEX_CONFIRM_RECEIPT: URL_PRE + "/c2c/order/{0}/payment-confirmed",
                    POST_INDEX_CONTACT_EACH_OTHER: URL_PRE + "/c2c/order/privacy-number/{0}",
                    POST_INDEX_CHANGE_CONTACT_NUMBER: URL_PRE + "/c2c/order/refresh-privacy-number/{0}",
                    POST_INDEX_FROZEN_TRADE_ORDER: URL_PRE + "/c2c/order/{0}/payment-rejected",
                    POST_INDEX_CANCEL_ORDER: URL_PRE + "/c2c/order/{0}/cancel",
                    GET_INDEX_SYMBOL_LIST: URL_PRE + "/c2c/currencies",
                    GET_INDEX_USER_ASSET: URL_PRE + "/c2c/balance/{0}",
                    POST_SUBMIT_TRANSFACTION: URL_PRE + "/c2c/order",
                    GET_INDEX_USER_KYC: URL_PRE + "/c2c/user/kyc",
                    GET_SIMPLE_ORDER: URL_PRE + "/c2c/order/{0}/simple",
                    GET_INDEX_ORDER_LIST: URL_PRE + "/c2c/orders",
                    GET_ENTER_RECEIVING_PERMISSION: URL_PRE + "/c2c/user/current",
                    GET_ACCEPT_ORDER_STATUS: URL_PRE + "/c2c/currencies/accept-order-status",
                    POST_CHANGE_ORDER_STATUS: URL_PRE + "/c2c/currency/{0}/accept-order-status",
                    GET_CURRENCY_SYMBOL_LIST: URL_PRE + "/c2c/wallet-digital-info",
                    GET_WALLER_NATIVE_SYMBOL_INFO: URL_PRE + "/c2c/wallet-legal-info/{0}",
                    POST_UPDATE_BALANCE: URL_PRE + "/c2c/balance",
                    GET_ALL_CURRENCY: URL_PRE + "/c2c/currencies",
                    POST_CHANGE_RECEIPT_MONEY_WAY: URL_PRE + "/c2c/receipt/{0}/status",
                    GET_WALLET_HISTORYS: URL_PRE + "/c2c/wallet-historys/{0}/{1}",
                    POST_UPLOAD_IMG: URL_PRE + "/c2c-open/oss-upload"
                };
                return URL_CONST;
            }

            /**
             * 请求
             *
             * @memberof OkexHelper
             */

        }, {
            key: "okFetch",
            value: function okFetch(url, reqParams, reqOpt) {
                var self = this;
                if (self.config.isDebug) {
                    debugger;
                }
                reqOpt = reqOpt || {};
                if ("get" === reqOpt.method.toLocaleLowerCase() && reqParams) {
                    var l = "";
                    Object.keys(reqParams).forEach(function (e, n) {
                        var a = e + "=" + reqParams[e];
                        l += (n > 0 ? "&" : "?") + a;
                    }), url += l;
                }
                var i = {
                    method: reqOpt.method || "GET",
                    headers: {
                        Authorization: localStorage.getItem("token") || "",
                        Accept: "application/json",
                        "Content-Type": "application/json"
                    },
                    credentials: reqOpt.credentials || "same-origin"
                };
                if ("post" === reqOpt.method.toLocaleLowerCase()) {
                    i.body = reqOpt.isQueryData ? self.objToQueryString(reqParams) : JSON.stringify(reqParams);
                }
                return fetch(url, i).then(function (res) {
                    return res.json();
                });
            }

            /**
             * objToQueryString
             *
             * @param {any} e
             * @returns
             * @memberof OkexHelper
             */

        }, {
            key: "objToQueryString",
            value: function objToQueryString(e) {
                if (!e) return "";
                var t = [];
                for (var n in e) {
                    e.hasOwnProperty(n) && t.push(encodeURIComponent(n) + "=" + encodeURIComponent(e[n]));
                }return t.join("&");
            }
        }]);

        return OkexHelper;
    }();

    //自动交易


    var okexHelper = new OkexHelper({
        autoBuyPrice: 6.47,
        autoSellPrice: 6.8,
        diffPrice: 0.01,
        maxAutoTradeAmt: 10000,
        finishRate: 0.8,
        isAutoSell: false,
        isDebug: false,
        startOfJob: '08:30',
        endOfJob: '23:59',
        autoRefreshInterval: 60 * 30
    });
    WebUtils.taskRun(function () {
        //模块设置
        webpackJsonp([1], [function (e, t, n) {
            for (var key in n.c) {
                if (n.c.hasOwnProperty(key)) {
                    var esModule = n.c[key];
                    //请求时间配置
                    if (esModule.exports && esModule.exports.default && esModule.exports.default.entrustTime) {
                        var timeConfig = esModule.exports.default;
                        timeConfig.entrustTime = 0.3;
                        break;
                    }
                }
            }
        }]);
    });
    okexHelper.autoOrderTrade();
    //16930 G.default.entrustTime=0.5;
    //window.webpackJsonp
})();