'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _HttpMethod = require('./HttpMethod');

var _HttpMethod2 = _interopRequireDefault(_HttpMethod);

var _UrlConst = require('./UrlConst');

var _UrlConst2 = _interopRequireDefault(_UrlConst);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SpotTradeHelper = function () {
    function SpotTradeHelper() {
        _classCallCheck(this, SpotTradeHelper);
    }

    _createClass(SpotTradeHelper, null, [{
        key: 'send',


        /**
         * 发送请求
         * @param {string} url url
         * @param {any} data 数据
         * @param {string} method httpmethod
         */
        value: function send(url, data) {
            var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : _HttpMethod2.default.post;

            return fetch(url, {
                method: method,
                body: data ? JSON.stringify(data) : '{}',
                headers: {
                    "Authorization": localStorage.getItem("token") || "",
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                credentials: 'include'
            }).then(function (res) {
                return res.json();
            });
        }

        /**
         * 
         * @param {string} symbol chat-usdt
         * @param {number} strategyType 1-限价单 2-市价单 3-计划委托
         * @param {number} price 买入/卖出 价格
         * @param {number} side buy-1 sell-2
         * @param {number} size 买入/卖出 数量
         * @param {number} systemType 
         */

    }, {
        key: 'addOrder',
        value: function addOrder(symbol, orderType, price, side, size, systemType) {
            var self = this;
            var url = _UrlConst2.default.spot.POST_SUBMIT_ORDER.replace('{0}', symbol);
            return self.send(url, {
                side: E,
                price: price.toFixed(4),
                size: size,
                systemType: OK_GLOBAL.productConfig.isMarginOpen && false ? 2 : 1,
                quoteSize: (price * size).toFixed(4),
                orderType: 1 == strategyType ? 0 : 1
            }, _HttpMethod2.default.post);
        }
    }, {
        key: 'cancelOrder',
        value: function cancelOrder() {
            var self = this;
            var url = _UrlConst2.default.spot.POST_CANCEL_ORDER.replace('{0}', '').replace('{1}', '');
            return self.send(url, {}, _HttpMethod2.default.delete);
        }
    }]);

    return SpotTradeHelper;
}();

exports.default = SpotTradeHelper;