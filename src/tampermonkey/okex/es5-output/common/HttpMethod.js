'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var HttpMethod = {
    get: 'GET',
    post: 'POST',
    delete: 'DELETE'
};

exports.default = HttpMethod;