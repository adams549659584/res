"use strict";

var baseUrl = location.protocol + "//" + location.host + "/v2";
var UrlConst = {
    spot: {
        LOG_RECORD: "/logRecord",
        POST_COLLECT_PRODUCT: baseUrl + "/spot/new-collect/edit",
        GET_INDEX_TICKER: baseUrl + "/futures/market/indexTicker?symbol=f_usd_{0}",
        GET_IS_TRADEPWD: baseUrl + "/spot/config/is-need-trade-pwd",
        GET_PRODUCT_LIST: baseUrl + "/spot/new-collect",
        GET_CURRENCY: baseUrl + "/spot/markets/currencies",
        GET_ASSETS: baseUrl + "/spot/accounts/by-currency",
        GET_TICKERS: baseUrl + "/spot/markets/tickers",
        GET_DEPTH: baseUrl + "/spot/markets/deep-deal?symbol={0}",
        GET_UNSETTLEMENT: baseUrl + "/spot/order/unsettlement",
        GET_ORDER_HISTORY: baseUrl + "/spot/order/history",
        GET_ENTRUST_PLAN: baseUrl + "/spot/plan/recent",
        GET_HISTORY_ENTRUST: baseUrl + "/spot/plan/history",
        POST_SUBMIT_ORDER: baseUrl + "/spot/order/add?symbol={0}",
        POST_SUBMIT_PlAN_ORDER: baseUrl + "/spot/plan/add-entrust",
        POST_CANCEL_ORDER: baseUrl + "/spot/order/cancel?symbol={0}&orderId={1}",
        DELETE_CANCEL_ENTRUST: baseUrl + "/spot/plan/cancel?id={1}&symbol={0}",
        POST_CANCELALL_ORDER: baseUrl + "/spot/order/cancel-all?symbol={0}&systemType={1}",
        GET_CALLMARKET_BYID: baseUrl + "/spot/markets/call-auction?productId={0}",
        GET_KLINE_DATA: baseUrl + "/spot/markets/kline?size=300&symbol={0}&type={1}",
        GET_FULLKLINE_DATA: baseUrl + "/spot/markets/kline?since=0&symbol={0}",
        GET_DEALS: baseUrl + "/spot/markets/deals?symbol={0}",
        GET_MARGIN_HISTORY_ORDER: baseUrl + "/spot/margin/borrow-order/borrows",
        GET_BILLS: baseUrl + "/spot/bills/bills",
        GET_BILLS_TYPE: baseUrl + "/spot/bills/types",
        GET_MARGIN_BILLS: baseUrl + "/spot/margin/bills",
        GET_MARGIN_BILLS_TYPE: baseUrl + "/spot/margin/bills/types",
        POST_SUBMIT_AGREEMENT: baseUrl + "/spot/margin/settings/status",
        GET_USER_MARGIN_SETTING: baseUrl + "/spot/margin/settings/status",
        GET_LOAN_MAX_LIMIT: baseUrl + "/spot/margin/accounts/max-loan-limit",
        POST_SUBMIT_LOAN: baseUrl + "/spot/margin/accounts/loan",
        POST_SUBMIT_REPAYMENT: baseUrl + "/spot/margin/accounts/repayment",
        GET_MARGIN_ACCOUNT: baseUrl + "/spot/margin/accounts",
        GET_MARGIN_ACCOUNT_BYPRODUCT: baseUrl + "/spot/margin/accounts/{0}",
        GET_ONE_MERGETYPES: baseUrl + "/spot/merge/coin-deep-level-data",
        GET_INTRODUCE: baseUrl + "/spot/markets/currency/describe?currency={0}",
        DOWNLOAD_BILLS: baseUrl + "/spot/bills/download-bills"
    },
    c2c: {}
};

module.exports = UrlConst;