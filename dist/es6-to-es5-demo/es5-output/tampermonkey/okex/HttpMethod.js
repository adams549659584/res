'use strict';

var HttpMethod = {
    get: 'GET',
    post: 'POST',
    delete: 'DELETE'
};

module.exports = HttpMethod;