'use strict';

// ==UserScript==
// @name         syncManage
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        *://*manage.360kad.com/*
// @require      https://adams549659584.gitee.io/res/dist/gm-layui/layui.js?t=201805171458
// @grant GM_getResourceText
// @grant GM_getResourceURL
// @grant unsafeWindow
// @grant GM_xmlhttpRequest
// @connect *://tstmanage.360kad.com/*
// @connect *://rcmanage.360kad.com/*
// @connect *://manage.360kad.com/*
// @run-at document-start
// ==/UserScript==

(function () {
    'use strict';

    var resDomain = 'https://adams549659584.gitee.io/res/dist/gm-layui/';
    //配置
    layui.config({
        dir: resDomain, //layui.js 所在路径（注意，如果是script单独引入layui.js，无需设定该参数。），一般情况下可以无视
        version: false, //一般用于更新模块缓存，默认不开启。设为true即让浏览器不缓存。也可以设为一个固定的值，如：201610
        debug: false, //用于开启调试模式，默认false，如果设为true，则JS模块的节点会保留在页面
        base: '/res/gm-layui/' //设定扩展的Layui模块的所在目录，一般用于外部模块扩展
    });
    layui.use(['util', 'layer', 'form'], function () {
        var util = layui.util;
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        //添加样式
        $('head').append('<link rel="stylesheet" href="' + resDomain + 'css/layui.css">');
        //固定块
        util.fixbar({
            bar1: '&#xe609;',
            css: {
                top: 100,
                right: 10
            },
            bgcolor: '#0096df',
            click: function click(type) {
                var self = this;
                if (type === "bar1") {
                    //Promise 封装 请求
                    var kadPost = function kadPost(url, data) {
                        return new Promise(function (resolve, reject) {
                            GM_xmlhttpRequest({ //获取列表
                                method: "POST",
                                url: url,
                                headers: {
                                    "Content-Type": "application/json"
                                },
                                data: JSON.stringify(data),
                                responseType: 'json',
                                onload: function onload(response) {
                                    try {
                                        resolve(JSON.parse(response.responseText));
                                    } catch (error) {
                                        reject(error);
                                    }
                                },
                                onerror: function onerror() {
                                    reject(arguments);
                                }
                            });
                        });
                    };

                    //对应域
                    var thatOrigin = 'http://tstmanage.360kad.com';
                    var toOrigin = 'http://manage.360kad.com';

                    //布局分组
                    // const ktmLayOutGroupUrl = '/CMS/KTMLayoutGroup/Query';
                    // const ktmLayOutGroupData = {
                    //     "filters": [{
                    //         "field": "IsDelete",
                    //         "whereType": "Equal",
                    //         "value": "0"
                    //     }, {
                    //         "field": "PlatForm",
                    //         "whereType": "Equal",
                    //         "value": "ktm"
                    //     }],
                    //     "sorts": [{
                    //         "field": "Id",
                    //         "isAsc": false
                    //     }],
                    //     "dbKey": null,
                    //     "entityType": null,
                    //     "page": 1,
                    //     "pageSize": 9999
                    // };
                    // const ktmAddLayoutGroupUrl = '/CMS/KTMLayoutGroup/Add';

                    // kadPost(`${thatOrigin}${ktmLayOutGroupUrl}`, ktmLayOutGroupData)
                    //     .then(thatRes => {
                    //         layer.msg(`加载【${thatOrigin}】布局模板分组信息完毕`);
                    //         return thatRes;
                    //     })
                    //     .then((thatRes) => {
                    //         if (thatRes.Rows && thatRes.Rows.length > 0) {
                    //             thatRes.Rows.forEach((row) => {
                    //                 const addKtmLayoutGroupData = {
                    //                     "Id": row.Id,
                    //                     "Name": row.Name,
                    //                     "Description": row.Description
                    //                 };
                    //                 kadPost(`${toOrigin}${ktmAddLayoutGroupUrl}`, addKtmLayoutGroupData)
                    //                     .then((addResult) => {
                    //                         layer.msg(addResult.Message);
                    //                     })
                    //                     .catch((reason) => {
                    //                         console.error(reason);
                    //                     });
                    //             });
                    //         }
                    //     })
                    //     .catch((reason) => {
                    //         console.error(reason);
                    //     });

                    //页面分组
                    // const ktmPageGroupUrl = '/CMS/KTMPageGroup/Query';
                    // const ktmPageGroupData = { "filters": [{ "field": "IsDelete", "whereType": "Equal", "value": "0" }, { "field": "PlatForm", "whereType": "Equal", "value": "ktm" }], "sorts": [{ "field": "Id", "isAsc": false }], "dbKey": null, "entityType": null, "page": 1, "pageSize": 9999 };
                    // const ktmAddPageGroupUrl = '/CMS/KTMPageGroup/Add';
                    // kadPost(`${thatOrigin}${ktmPageGroupUrl}`, ktmPageGroupData)
                    //     .then((thatRes) => {
                    //         layer.msg(`加载【${thatOrigin}】页面分组信息完毕`);
                    //         return thatRes;
                    //     })
                    //     .then((thatRes) => {
                    //         if (thatRes.Rows && thatRes.Rows.length > 0) {
                    //             thatRes.Rows.forEach((row) => {
                    //                 const addKtmPageGroupData = {
                    //                     "Id": row.Id,
                    //                     "Name": row.Name,
                    //                     "Description": row.Description
                    //                 };
                    //                 kadPost(`${toOrigin}${ktmAddPageGroupUrl}`, addKtmPageGroupData)
                    //                     .then((addResult) => {
                    //                         layer.msg(`${addKtmPageGroupData.Id}:${addResult.Message}`);
                    //                     })
                    //                     .catch((reason) => {
                    //                         console.error(reason);
                    //                     });
                    //             });
                    //         }
                    //     })
                    //     .catch(reason => {
                    //         console.error(reason);
                    //     });


                    //页面
                    // const ktmPageUrl = '/CMS/KTMPage/Query';
                    // const ktmPageData = { "filters": [{ "whereType": "equal", "field": "IsVisualPage", "value": "0" }, { "whereType": "equal", "field": "IsDelete", "value": "0" }, { "field": "SiteId", "whereType": "Equal", "value": "ktm" }, { "field": "PlatForm", "whereType": "Equal", "value": "ktm" }], "sorts": [], "dbKey": null, "entityType": null, "page": 1, "pageSize": 9999 };
                    // const ktmAddPageUrl = '/CMS/KTMPage/Add';
                    // kadPost(`${thatOrigin}${ktmPageUrl}`, ktmPageData)
                    //     .then((thatRes) => {
                    //         layer.msg(`加载【${thatOrigin}】页面分组信息完毕`);
                    //         return thatRes;
                    //     })
                    //     .then((thatRes) => {
                    //         if (thatRes.Rows && thatRes.Rows.length > 0) {
                    //             thatRes.Rows.forEach((row) => {
                    //                 const ktmPageFormDataUrl = '/CMS/KTMPage/GetFormData';
                    //                 const ktmPageFormData = { "Id": row.Id, "CateName": row.CateName };
                    //                 kadPost(`${thatOrigin}${ktmPageFormDataUrl}`, ktmPageFormData)
                    //                     .then((data) => {
                    //                         const addKtmPageData = {
                    //                             "SiteId": data.SiteId,
                    //                             "Id": data.Id,
                    //                             "PageGroup": data.PageGroup,
                    //                             "Name": ktmPageFormData.CateName,
                    //                             "DataProvider": data.DataProvider,
                    //                             "LayoutId": data.LayoutId,
                    //                             "Description": data.Description,
                    //                         };
                    //                         kadPost(`${toOrigin}${ktmAddPageUrl}`, addKtmPageData)
                    //                             .then((addResult) => {
                    //                                 layer.msg(`${addKtmPageData.Id}:${addResult.Message}`);
                    //                             })
                    //                             .catch((reason) => {
                    //                                 console.error(reason);
                    //                             });
                    //                     });
                    //             });
                    //         }
                    //     })
                    //     .catch(reason => {
                    //         console.error(reason);
                    //     });
                }
            }
        });
    });
})();