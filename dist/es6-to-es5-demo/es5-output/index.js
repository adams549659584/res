'use strict';

(function () {
    //非调试目录自动加载layui
    if (!location.href.includes('localhost')) {
        var layuiStyle = document.createElement('link');
        layuiStyle.rel = 'stylesheet';
        layuiStyle.href = './layui/css/layui.css';
        document.body.appendChild(layuiStyle);
        var layuiScript = document.createElement('script');
        layuiScript.src = './layui/layui.js';
        document.body.appendChild(layuiScript);
    }
})();

// const resBaseDir = 'http://localhost:9000/';

// layui.config({
//     dir: resBaseDir + 'layui/', //layui.js 所在路径（注意，如果是script单独引入layui.js，无需设定该参数。），一般情况下可以无视
//     version: false, //一般用于更新模块缓存，默认不开启。设为true即让浏览器不缓存。也可以设为一个固定的值，如：201610
//     debug: false, //用于开启调试模式，默认false，如果设为true，则JS模块的节点会保留在页面
//     base: '/res/layui/', //设定扩展的Layui模块的所在目录，一般用于外部模块扩展
// });
// layui.use(['util', 'layer', 'element'], () => {
//     const util = layui.util;
//     const $ = layui.jquery;
//     const layer = layui.layer;
//     //固定块
//     // util.fixbar({
//     //     bar1: true,
//     //     bar2: true,
//     //     css: {
//     //         right: 50,
//     //         bottom: 100
//     //     },
//     //     bgcolor: '#393D49',
//     //     click: (type) => {
//     //         if (type === 'bar1') {
//     //             layer.msg('icon是可以随便换的')
//     //         } else if (type === 'bar2') {
//     //             layer.msg('两个bar都可以设定是否开启')
//     //         }
//     //     }
//     // });
// });