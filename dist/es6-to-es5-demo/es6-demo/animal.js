class Animal {
    constructor() {
        console.log("==constructor animal==");
    }

    sayHello() {
        console.log("==sayHello animal==");
    }
}

export default Animal;