import HttpMethod from './HttpMethod';
import UrlConst from './UrlConst';

export default class SpotTradeHelper {

    /**
     * 发送请求
     * @param {string} url url
     * @param {any} data 数据
     * @param {string} method httpmethod
     */
    static send(url, data, method = HttpMethod.post) {
        return fetch(url, {
            method: method,
            body: data ? JSON.stringify(data) : '{}',
            headers: {
                "Authorization": localStorage.getItem("token") || "",
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            credentials: 'include'
        }).then(res => res.json());
    }

    /**
     * 
     * @param {string} symbol chat-usdt
     * @param {number} strategyType 1-限价单 2-市价单 3-计划委托
     * @param {number} price 买入/卖出 价格
     * @param {number} side buy-1 sell-2
     * @param {number} size 买入/卖出 数量
     * @param {number} systemType 
     */
    static addOrder(symbol, orderType, price, side, size, systemType) {
        const self = this;
        const url = UrlConst.spot.POST_SUBMIT_ORDER.replace('{0}', symbol);
        return self.send(url, {
            side: E,
            price: price.toFixed(4),
            size: size,
            systemType: OK_GLOBAL.productConfig.isMarginOpen && false ? 2 : 1,
            quoteSize: (price * size).toFixed(4),
            orderType: 1 == strategyType ? 0 : 1
        }, HttpMethod.post);
    }

    static cancelOrder() {
        const self = this;
        const url = UrlConst.spot.POST_CANCEL_ORDER.replace('{0}', '').replace('{1}', '');
        return self.send(url, {}, HttpMethod.delete);
    }
}