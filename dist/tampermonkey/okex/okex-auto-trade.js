// ==UserScript==
// @name         okex-auto-trade
// @namespace    https://gitee.com/adams549659584/res/tree/master/src/tampermonkey/okex
// @version      1.0.0
// @description  okex 自动交易
// @author       罗君
// @match        *://www.okex.com/spot/trade
// @require      https://adams549659584.gitee.io/res/dist/gm-layui/layui.js?t=201805211154
// @resource     frm-okex-setting http://adams549659584.gitee.io/res/dist/gm-layui/template/frm-okex-setting.html?t=201805211154
// @resource     raw-websocket http://adams549659584.gitee.io/res/dist/tampermonkey/okex/raw-websocket.js?t=201805211154
// @resource     new-websocket http://adams549659584.gitee.io/res/dist/tampermonkey/okex/new-websocket.js?t=201805211154
// @grant GM_getResourceText
// @grant GM_getResourceURL
// @grant unsafeWindow
// @run-at document-start
// ==/UserScript==

(function () {
    'use strict';

    // console.log(document.head);
    const newWebsocketUrl = GM_getResourceURL('new-websocket');
    // console.log(`newWebsocketUrl:${newWebsocketUrl}`);
    const newWebsocketScript = document.createElement('script');
    newWebsocketScript.type = 'text/javascript';
    newWebsocketScript.src = newWebsocketUrl;
    document.head.appendChild(newWebsocketScript);

    function taskRun(func) {
        return new Promise((resolve, reject) => {
            if (typeof func !== 'function') {
                reject(`函数有误`);
                return;
            }
            setTimeout(() => {
                try {
                    const thatArgs = [...arguments];
                    const funcArgs = thatArgs.splice(1);
                    resolve(func(...funcArgs));
                } catch (error) {
                    reject(error);
                }
            }, 0);
        });
    }

    let diffPrice = 0.1;
    const okexAsks = new Map();
    const okexBids = new Map();
    let okexAskPrices = [];
    let okexBidPrices = [];
    let isFirstLoad = true;

    const calcDepth = function (res) {
        if (isFirstLoad) {
            //sell
            var sellListLi = document.querySelectorAll('.sell-list li');
            for (let i = 0, len = sellListLi.length; i < len; i++) {
                const sellLi = sellListLi[i];
                const sellPrice = Number(sellLi.children[0].innerHTML.replace(/,/, ''));
                const sellTotalSize = Number(sellLi.children[1].innerHTML.replace(/,/, ''));
                if (sellTotalSize > 0) {
                    okexAsks.set(sellPrice, sellTotalSize);
                }
            }
            okexAskPrices = Array.from(okexAsks.keys()).sort();
            //buy
            var buyListLi = document.querySelectorAll('.buy-list li');
            for (let i = 0, len = buyListLi.length; i < len; i++) {
                const buyLi = buyListLi[i];
                const buyPrice = Number(buyLi.children[0].innerHTML.replace(/,/, ''));
                const buyTotalSize = Number(buyLi.children[1].innerHTML.replace(/,/, ''));
                if (buyTotalSize > 0) {
                    okexBids.set(buyPrice, buyTotalSize);
                }
            }
            okexBidPrices = Array.from(okexBids.keys()).sort();
        }
        var data = res.data;
        taskRun(() => {
            if (data.asks && data.asks.length > 0) {
                for (let i = 0, len = data.asks.length; i < len; i++) {
                    const ask = data.asks[i];
                    const price = Number(ask.price);
                    const totalSize = Number(ask.totalSize);
                    if (totalSize === 0) {
                        okexAsks.delete(price);
                    } else {
                        okexAsks.set(price, totalSize);
                    }

                    okexAskPrices = Array.from(okexAsks.keys()).sort();
                    const thatDiffPrice = (okexAskPrices[0] * 10000 - okexBidPrices[okexBidPrices.length - 1] * 10000) / 10000;
                    if (thatDiffPrice >= diffPrice) {
                        const thatBidPrice = okexBidPrices[okexBidPrices.length - 1];
                        const thatBidSize = okexBids.get(thatBidPrice);
                        const thatAskPrice = okexAskPrices[0];
                        const thatAskSize = okexAsks.get(thatAskPrice);
                        console.log(`买入价:${thatBidPrice},数量:${thatBidSize},卖出价：${thatAskPrice},数量:${thatAskSize},差价：${thatDiffPrice}`);
                        console.log(okexAskPrices);
                        console.log(okexBidPrices);
                    }
                }
            }
        });

        taskRun(() => {
            if (data.bids && data.bids.length > 0) {
                for (let i = 0, len = data.bids.length; i < len; i++) {
                    const bid = data.bids[i];
                    const price = Number(bid.price);
                    const totalSize = Number(bid.totalSize);
                    if (totalSize === 0) {
                        okexBids.delete(price);
                    } else {
                        okexBids.set(price, totalSize);
                    }
                    okexBidPrices = Array.from(okexBids.keys()).sort();
                    const thatDiffPrice = (okexAskPrices[0] * 10000 - okexBidPrices[okexBidPrices.length - 1] * 10000) / 10000;
                    if (thatDiffPrice >= diffPrice) {
                        const thatBidPrice = okexBidPrices[okexBidPrices.length - 1];
                        const thatBidSize = okexBids.get(thatBidPrice);
                        const thatAskPrice = okexAskPrices[0];
                        const thatAskSize = okexAsks.get(thatAskPrice);
                        console.log(`买入价:${thatBidPrice},数量:${thatBidSize},卖出价：${thatAskPrice},数量:${thatAskSize},差价：${thatDiffPrice}`);
                        console.log(okexAskPrices);
                        console.log(okexBidPrices);
                    }
                }
            }
        });
    };


    var resDomain = 'https://adams549659584.gitee.io/res/dist/gm-layui/';
    //配置
    layui.config({
        dir: resDomain, //layui.js 所在路径（注意，如果是script单独引入layui.js，无需设定该参数。），一般情况下可以无视
        version: false, //一般用于更新模块缓存，默认不开启。设为true即让浏览器不缓存。也可以设为一个固定的值，如：201610
        debug: false, //用于开启调试模式，默认false，如果设为true，则JS模块的节点会保留在页面
        base: '/res/gm-layui/', //设定扩展的Layui模块的所在目录，一般用于外部模块扩展
    });
    layui.use(['util', 'layer', 'form'], function () {
        var util = layui.util;
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        // $(function () {
        //     const websocketScriptName = 'websocket.js';
        //     const lastWebsocketScriptSrc = 'https://img.bafang.com/cdn/exv1/20180509200029/spot/libs/websocket.js';

        //     for (let i = 0; i < document.scripts.length; i++) {
        //         const thatScript = document.scripts[i];
        //         console.log(`thatScript.src:${thatScript.src}`);
        //         if (thatScript.src.includes(websocketScriptName)) {
        //             //TODO 存原始的websocket 与 最新的比较
        //             //TODO 替换逻辑处理
        //             $(thatScript).before('<script type="text/javascript" src="https://adams549659584.gitee.io/res/dist/tampermonkey/okex/new-websocket.js"></script>').remove();
        //             break;
        //         }
        //     }
        // });
        //添加样式
        $('head').append('<link rel="stylesheet" href="' + resDomain + 'css/layui.css">');
        //固定块
        util.fixbar({
            bar1: '&#xe631;',
            css: {
                top: 100,
                right: 10
            },
            bgcolor: '#0096df',
            click: function (type) {
                var self = this;
                if (type === "bar1") {
                    const ws = OK_GLOBAL.ws;
                    // const ws = new WebSocketUtil();
                    if (ws.isInjected) {
                        //页面层
                        const formHtml = GM_getResourceText('frm-okex-setting');
                        layer.open({
                            title: '配置',
                            type: 1,
                            skin: 'layui-layer-rim', //加上边框
                            area: '800px', //宽高
                            content: formHtml
                        });
                        form.render();
                    } else {
                        layer.confirm('脚本注入失败，是否刷新重新注入', {
                            btn: ['刷新', '算了'] //按钮
                        }, function () {
                            location.reload();
                            return;
                        }, function () {
                            //移除配置
                            $(self).parent().remove();
                        });
                    }
                }
            }
        });
    });

    //全局
    unsafeWindow.calcDepth = calcDepth;
})();