const HttpMethod = {
    get: 'GET',
    post: 'POST',
    delete: 'DELETE'
};

export default HttpMethod;