const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = {
    title: 'hello,test', //html5文件中<title>部分
    fileName: 'index.html', // 默认是index.html，服务器中设置的首页是index.html，如果这里改成其它名字，那么devServer.index改为和它一样，最终完整文件路径是output.path+filename，如果filename中有子文件夹形式，如`./ab/cd/front.html`，只取`./front.html`
    template: './src/index.html', ////如果觉得插件默认生成的hmtl5文件不合要求，可以指定一个模板，模板文件如果不存在，会报错，默认是在项目根目录下找模板文件，才模板为样板，将打包的js文件注入到body结尾处
    inject: 'body' // true|body|head|false，四种值，默认为true,true和body相同,是将js注入到body结束标签前,head将打包的js文件放在head结束前,false是不注入，这时得要手工在html中加js
};

module.exports = {
    // entry可以为字符串|对象|数组三种形式
    // 字符串，适合spa,也就是单页网页，如手机网页
    // 下面这个entry最终的位置是 项目根目录/today/wang/app/entry.js
    // 前面./不能少，后面的.js可以省略，也可以写
    // 以下演示三种entry，实际中取一种就行
    entry: './src/index.js',
    // 数组
    //entry: ["./home.js","./about.js","./contact.js"],
    // 对象，适合于多入口网站，也就是mpa，对象格式的每个键，如home,about,contact是每个入口文件chunk的名字，字符串和数组没有键，它也有一个chunk，名字默认为main    
    //  entry: {
    //    home: "./home.js",
    //    about: "./about.js",
    //    contact: "./contact.js"
    //  }, 
    output: {
        ////最后生成的打包文件所在的目录，是一个绝对值，，如果不指定，表示当前目录。如果文件夹不存在，会自动创建
        //这个路径除了这里会用到之外，象html插件,file-loader加载器也会用到
        // 最后生成的打包文件是 path+ filename
        path: path.resolve(__dirname, '../dist'),
        filename: './app-[hash].js',
        //filename中可以使用[name],[id],[hash],[chunkhash][query]五种变量
        // filename中可以含子文件夹，如如filename: "a/b/c/[id]app.js"
        // filename中可以含子文件夹，如如filename: "a/b/c/[id]app.js"
        //filename: 'wang.js', // 如果entry是个对象且有多个chunkname，那么这里会报错，但会生成一个wang.js,它的内容是第一个chunk的，建议entry是多个chunk的对象时，不要写固定名字，要带[name]变量
        //filename: '[name]wang.js', // 此处的[name]与entry中的chunk名字对应，象上面entry是字符串和数组时，最后输出的文件名是mainwang.js，entry是对象，最后输出的文件名是 homewang.js,aboutwang.js,ccontact123wang.js
        //filename: '[id]wang.js', //id从0,1这么增长的，象上面会生成0wang.js,1wang.js,2wang.js三个文件
        //filename: "[name].[hash].bundle.js" //会打包成about.bab6d0fe556449a9229e.bundle,contact123.bab6d0fe556449a9229e.bundle,home.bab6d0fe556449a9229e.bundle，尤其要记住的是[hash]不要单独用，要与[name]或[id]配合用
        //filename: "[chunkhash].yes.js", //78f16d7b19ff7ec1fd3a.yes.js,e12898a66041f68c1959.yes.js,f590b1f2de7b72dea5b3.yes.js，20位hash值
        hashDigestLength: 16 //指定最后chunkhash和hash变量生成字符串的长度，默认是20个字符
    },
    module: {
        rules: [{
                test: /\.html$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: ['data-src']
                    }
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]', //useRelativePath:true,
                        outputPath: 'img/' //后面的/不能少
                    }
                }]
            }
        ]
    },
    resolve: {
        //扩展名为.js,.vue,.json的可以忽略，如 import App from './app'，先在当前目录中找app.js，没有再找app.vue，没找到，再找.json，如果还没找到，报错
        extensions: ['.js', '.vue', '.json'],
        alias: {
            //别名，这是一个正则的写法，表示以vue结尾的，如import Vue from 'vue' 表示 import Vue from 'vue/dist/vue.esm.js'
            'vue$': 'vue/dist/vue.esm.js',
            //这也是为懒人服务的,import HelloWorld from '@/components/HelloWorld'这里的@其实就是代表src这个目录 
            '@': path.resolve('src'),
            //import Table from '#/table'
            '#': path.resolve('src/ui/components')
        }
    },
    plugins: [
        new HtmlWebpackPlugin(HtmlWebpackPluginConfig),
        // new webpack.DefinePlugin({
        //     BJ: JSON.stringify('北京'), // 内置插件，无须安装，可以理解为它是webpack实例的一个方法，该插件相当于apache等web服务器上定义一个常量
        // })
    ],
    devServer: {
        //progress只在命令行用，不在配置文件中配
        //网站的根目录为 根目录/dist
        contentBase: path.join(__dirname, '../dist'),
        port: 9000, //端口改为9000
        open: true, // 自动打开浏览器
        index: 'index.html', // 与HtmlWebpackPlugin中配置filename一样
        inline: true, // 默认为true, 意思是，在打包时会注入一段代码到最后的js文件中，用来监视页面的改动而自动刷新页面,当为false时，网页自动刷新的模式是iframe，也就是将模板页放在一个frame中
        hot: false,
        //它与output.publicPath的值应该是一样的，值为上面contentBase目录的子目录，是放js,css,图片等资源的文件夹，记得打包时，将图片等拷贝或打包到该文件下
        // publicPath: '/static456/',
        compress: true //压缩
    },
    // 会监视被导入的文件是否有改动，如果有改动，自动打包，但配置文件的改动不会被监视
    watch:true,
}