const gulp = require('gulp');
const babel = require('gulp-babel');
const es2015 = require('babel-preset-es2015');
const gulpWebpack = require('gulp-webpack');
const path = require('path');
const webserver = require('gulp-webserver');
const url = require('url');
const webpack = require('webpack');
const webpackConfig = require('./build/webpack.test.conf');
const uglify = require('gulp-uglify');

const basePath = {
    src: "src",
    esTranslateDemoPath: 'src/es6-to-es5-demo/'
}

gulp.task('es6-to-es5-demo', () => {
    // gulp.src(path.join(basePath.esTranslateDemoPath, '/es6-demo/**/*'))
    // gulp.src(path.join(basePath.esTranslateDemoPath, '/es6-demo/dog.js'))
    gulp.src('./src/**/*.js')
        .pipe(babel({
            presets: [es2015]
        }))
        .pipe(gulp.dest(path.join(basePath.esTranslateDemoPath + '/es5-output'))) ////es6转js必须在webpack之前，否则webpack找不到要包装的js会报错
        .pipe(gulpWebpack({ //babel编译import会转成require，webpack再包装以下代码让代码里支持require
            output: {
                filename: 'app.js',
            },
            status: {
                colors: true
            }
        }))
        .pipe(gulp.dest(path.join(basePath.esTranslateDemoPath + "/es5-bundle"))); //包装好的js目录
});

gulp.task('okex-build', () => {
    // gulp.src(path.join(basePath.esTranslateDemoPath, '/es6-demo/**/*'))
    // gulp.src(path.join(basePath.esTranslateDemoPath, '/es6-demo/dog.js'))
    gulp.src(['./src/tampermonkey/okex/**/*.js', '!./src/tampermonkey/okex/ignore/**/*'])
        .pipe(babel({
            presets: [es2015]
        }))
        .pipe(gulp.dest(path.join('./src/tampermonkey/okex' + '/es5-output'))) ////es6转js必须在webpack之前，否则webpack找不到要包装的js会报错
        .pipe(gulpWebpack({ //babel编译import会转成require，webpack再包装以下代码让代码里支持require
            output: {
                filename: 'bundle.js',
            },
            status: {
                colors: true
            }
        }))
        .pipe(uglify()) //压缩
        .pipe(gulp.dest(path.join('./src/tampermonkey/okex/' + "/es5-bundle"))); //包装好的js目录
});

gulp.task('webserver', () => {
    gulp.src('dist')
        .pipe(webserver({
            port: 9000, //端口
            host: "localhost", //网络服务器的主机名
            //true/false 或 在浏览器中打开本地主机服务器。通过提供一个字符串，你可以指定打开的路径（完整路径，使用完整的url http://my-server:8080/public/）。
            // open: true,
            open: '/index.html',
            //文件回退到（相对于Web服务器根目录）
            // fallback: 'index.html',
            //访问的路径是否显示
            liveload: true, //实时刷新代码
            directoryListing: {
                //从哪个目录下开始启动
                path: 'dist',
                enable: true
            },
            //是否使用https。默认情况下，gulp-webserver为您提供一个开发证书，但您可以自由指定密钥和证书的路径，方法是提供如下所示的对象：{key: 'path/to/key.pem', cert: 'path/to/cert.pem'}。
            https: true,
            //对请求进行拦截
            middleware: function (req, res, next) {
                //req:发送的请求
                //res:需要接受响应的对象
                //next:指向下一步操作的指针
                const urlObj = url.parse(req.url, true);
                console.log(urlObj.pathname);
                if (urlObj.pathname == '/data/json.json') {
                    //设置响应头
                    res.setHeader('Content-Type', 'application/json');
                    //读取文件
                    fs.readFile('json/data.json', 'utf-8', function (err, data) {
                        //将文件的数据设置为响应的数据
                        res.end(data);
                    });
                    return;
                }
                //放网页继续进行
                next();
            },
            //代理对象列表。每个代理对象都可以通过指定
            // proxies: {
            //     source: '/abc',
            //     target: 'http://localhost:8080/abc',
            //     options: {
            //         headers: {
            //             'ABC_HEADER': 'abc'
            //         }
            //     }
            // }
        }));
});

gulp.task('copy-file', () => {
    gulp.src('src/**/*')
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', () => {
    gulp.watch('src/**/*', ['copy-file']);
});

gulp.task('build-js', () => {
    gulpWebpack(webpackConfig)
        .pipe(gulp.dest(path.join(basePath.esTranslateDemoPath, '/es5-bundle')));
});

gulp.task('default-dev', ['copy-file', 'webserver', 'watch']);
gulp.task('default-dist', ['copy-file']);